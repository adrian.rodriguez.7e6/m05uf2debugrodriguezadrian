﻿/*
 Author: Adrián Rodríguez Bolín
 Date:7/12/2022
 Description: Solucionar el codi.
 */

using System;

namespace DebugFor
{
    class DebugFor
    {
        static void Main()
        {
            long numero1 = 548745184;
            long numero2 = 25145;
            long result = 0;
            for (int i = 0; i < numero2; i++)
            {
                result += numero1;
            }
            
            Console.WriteLine("La multiplicació de {0} i {1} es {2}", numero1, numero2, result);
        }

    }
}
