﻿/*
 Author: Adrián Rodríguez Bolín
 Date:7/12/2022
 Description: Solucionar els errors sintàctics i semàntics via depurador d'un programa que faci el següent: Ens donen 2 colors i retornem quin equip és.
 */

using System;
using System.Drawing;

namespace TeamColor
{
    internal class TeamColor
    {
        static void Main()
        {
            string color1 = Console.ReadLine();
            string color2 = Console.ReadLine();
            if (color1 == "white")
            {
                if (color2 == "green") Console.WriteLine("Team1");
                if (color2 == "blue") Console.WriteLine("Team2");
                if (color2 == "brown") Console.Write("Team2");
            }
            if (color1 == "red")
            {
                if (color2 == "blue")
                {
                    Console.WriteLine("Team3");
                }
                if (color2 == "black")
                {
                    Console.WriteLine("Team5");
                }
                if (color2 == "green")
                {
                    Console.WriteLine("Team6");
                }
            }
            if (color1 == "green")
            {
                if (color2 == "red")
                {
                    Console.WriteLine("Team6");
                }
            }
            else
            {
                Console.WriteLine("ERROR");
            }
        }

    }
}
