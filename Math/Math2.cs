﻿/*
 Author: Adrián Rodríguez Bolín
 Date:7/12/2022
 Description: Arreglar els warnings del programa, veure com funciona i contestar les preguntes.
 */



using System;

namespace Math2
{
    class Math2
    {
        static void Main()
        {
            var result = 0.0;
            for (int i = 0; i < 10000; i++)
            {
                if (result > 100)
                {
                    result = Math.Sqrt(result);
                    Console.WriteLine($"para i={i} hago raiz y te da:{result}");
                }

                if (result < 0)
                {
                    result += result * result;
                    Console.WriteLine($"para i={i} hago multiplicacion y te da:{result}");

                }

                result += 20.2;
                Console.WriteLine(result);
            }

            Console.WriteLine("El resultat és "+result);
            }

        //No arriba a i=10000, ja que es queda a i=9999 perque en el for posa i<10000.
        //El resultat solament és major a 110 quan el número de i acaba en 4 o en 9, pero mai és major que 120.
    }
}
